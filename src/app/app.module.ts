import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { D3Service } from 'd3-ng2-service';
import { AppComponent } from './app.component';
import { BarsComponent } from './component/bars/bars.component';
import { GaugeComponent } from './component/gauge/gauge.component';

@NgModule({
  declarations: [
    AppComponent,
    BarsComponent,
    GaugeComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [ D3Service ],
  bootstrap: [AppComponent]
})
export class AppModule { }
