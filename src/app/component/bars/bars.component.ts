import { Component, OnInit, ElementRef } from '@angular/core';
import { D3Service, D3, Selection } from 'd3-ng2-service';

@Component({
  selector: 'app-bars',
  templateUrl: './bars.component.html',
  styleUrls: ['./bars.component.css']
})
export class BarsComponent implements OnInit {

  constructor(private element: ElementRef, private d3Service: D3Service) { // <-- pass the D3 Service into the constructor
    this.d3 = d3Service.getD3(); // <-- obtain the d3 object from the D3 Service
    this.parentNativeElement = element.nativeElement;
  }


  parentNativeElement:any;
  d3:D3;

  sampleData = [
    {c:"Mario",v:10},
    {c:"Luca",v:12},
    {c:"Luigi",v:14},
    {c:"Alessandro",v:8},
    {c:"Maurizio",v:16},
    {c:"Marco",v:9},
  ]
  
  ngOnInit() {
    const d3 = this.d3;
    let selection : Selection<any,any,any,any>;
    if(this.parentNativeElement){
      selection = d3.select(this.parentNativeElement);

      const dataJoin = selection.select<SVGElement>(".chart_root")
        .selectAll<SVGRectElement,any>(".col")
        .data(this.sampleData);
      
      const Y = d3.scaleLinear()
        .domain([0,20])
        .range([0,200]);

      const labels = this.sampleData.map(d => d.c);

      const X = d3.scaleBand()
        .paddingInner(0.1)
        .domain(labels)
        .rangeRound([0,400])
      
      const color = d3.scaleOrdinal(d3.schemeCategory10)
        .domain(labels);


      

      dataJoin
        .enter()
        .append<SVGRectElement>("rect")
        .attr("class","col")
        .merge(dataJoin)
        .style("fill",(d)=>color(d.c))
        .attr("x",(d)=>X(d.c))
        .attr("y",(d)=> 200 - Y(d.v)  )
        .attr("width", X.bandwidth() - X.padding())
        .attr("height",(d)=> Y(d.v))

      dataJoin.exit().remove()
        
    }
  }



}
