import { Component, OnInit, AfterViewInit } from '@angular/core';
import * as c3 from 'c3';
import { Observable } from 'rxjs/Observable';
import { interval } from 'rxjs/observable/interval'
import { map, take } from 'rxjs/operators';

@Component({
  selector: 'app-gauge',
  templateUrl: './gauge.component.html',
  styleUrls: ['./gauge.component.css']
})
export class GaugeComponent implements OnInit, AfterViewInit {

  constructor() { }

  chart: c3.ChartAPI;

  eff= 80;
  dis=  5;
  ok = 90;

  ngOnInit() {
  }
  ngAfterViewInit(): void {
    const { eff, dis, ok } = this;
    this.chart = c3.generate({
      bindto: "#my-gauge",
      data: {
        columns: [
          ['Efficency', eff],
          ['Discard', dis],
          ['Good', ok], 
        ],
        onclick: (d,e)=>{console.log(d,e)},
        type: "gauge",
      }
    })

    const tick: Observable<number> = interval(2500).pipe(take(20));


    tick.subscribe( n => {
      
      let { eff, dis, ok } = this;

      eff = (eff * 4 + (80 + Math.random() * 10))/5;
      dis = (dis * 4 + (Math.random() * 10))/5;
      ok =  (ok * 4 + (90 + Math.random() * 10))/5;

      this.chart.load({
        columns: [
          ['Efficency', eff],
          ['Discard', dis],
          ['Good', ok]]
      })
      this.ok = ok;
      this.eff = eff;
      this.dis = dis;
    });

  }

}
